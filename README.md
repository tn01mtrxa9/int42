DASHBOARD FOR BICYCLE
ABSTRACT
As the name says, this project is about measurement of speed and distance that we travel or cover in our day to day life while we are riding our bicycle.
Dashboard not only gives the speed and distance information but you can also tell the fellow passenger about the signals i.e. while taking turn.
The basic need to the project can be for safety purpose.
This is not a compulsory need but still can be useful in day to day workout tables. 
Including all, the project is very user-friendly and can be modified in the way as per the requirement 
Various sensor and basic knowledge about microprocessors is enough to implemented.
Additional features that can be added are GPS location and Bluetooth transceiver.
